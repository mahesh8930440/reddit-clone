import Header from "./Components/Header"
import { ChakraProvider } from '@chakra-ui/react'
import Posts from "./pages/Post";
import SubReddit from "./pages/SubReddit";
import {createBrowserRouter,Outlet} from "react-router-dom"
import CreatePost from "./pages/CreatePost";
import {Box} from '@chakra-ui/react'
import Comments from "./pages/Comments";
function App() {
  return (
    <ChakraProvider>
      <Header />
      <Box minH={"90vh"}  bgImage={"https://blog.depositphotos.com/wp-content/uploads/2017/07/Soothing-nature-backgrounds-2.jpg.webp"}  backgroundSize="cover">
         <Outlet />
      </Box>
   
    </ChakraProvider>
    
  )
}
const appRouter=createBrowserRouter([
  {
    path:"/",
    element:<App/>,
    children:[
      {
        path:"/",
        element:<Posts/>      
      },
      {
        path:"/submit",
        element:<CreatePost />
      },
      {
        path:"/r/:id",
        element:<SubReddit/>
      },
      {
        path:"/r/:subredditName/comment/:postId",
        element:<Comments/>
      }
      
    ]
  }
])

export default appRouter;
