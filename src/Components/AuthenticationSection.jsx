import { FirebaseContext } from "../context/Firebase";
import {useContext} from "react";
import { signOut } from 'firebase/auth';
import {auth} from "../context/Firebase"
import {Menu,MenuButton,MenuItem,Image,Button,MenuList,HStack,Text, Avatar,Flex,useToast} from '@chakra-ui/react'
import { AddIcon,Icon} from '@chakra-ui/icons'
// import { FaHome } from "react-icons/fa";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { wrap } from "framer-motion";
import { GoSignOut } from "react-icons/go";
const AuthenticationSection= ()=>{
    const authData =useContext(FirebaseContext);
    const toast=useToast();
    const user=authData.user;
    
    const handleSignOut = async () => {
        
        try {
            await signOut(auth);
            toast({
                title: 'Logout',
                description: 'You have been successfully logged out.',
                status: 'success',
                duration: 2000,
                isClosable: true,
            })
            console.log("sign out success")
        } catch (error) {
            toast({
                title: 'Error',
                description: error.message,
                status: 'failed',
                duration: 2000,
                isClosable: true,
            })
            console.error('Error signing out:', error);
        }
    };
    const outline= {
          color:"gray",
          border: "1px solid",
          borderColor: "blue.500",
          borderRadius:"10px",
          padding:"4px",
          margin :"4px"
    }
    return (
        <>
            {user ? (
                <Menu width="80px">
                    <MenuButton as={Button}  pr="15px" pl="0px" flexWrap={"wrap"}>
                        <MenuItem><Avatar src={"https://images.pexels.com/photos/1704488/pexels-photo-1704488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"} height="20px" width="20px" mr="5px">
                            </Avatar>
                            <Text>{user.displayName}</Text>
                            </MenuItem>
                    </MenuButton>
                    <MenuList>
                        <MenuItem minH='48px'>
                            <GoSignOut mr="12px" size="30px"/>
                            <Button onClick={handleSignOut}>Sign Out</Button>
                        </MenuItem>
                    </MenuList>
                </Menu>
            ) : (
                <button onClick={authData.signInWithGoogle} style={outline}>Sign In</button>
            )}
        </>
    );
    
}
export default AuthenticationSection;