import React, { useContext, useEffect,useState } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  Heading,
  Text,
  Input,
  
} from '@chakra-ui/react';

import { ModalContext } from '../context/community';
import {db} from "../context/Firebase";
import { addDoc, collection ,getDocs,getDoc} from 'firebase/firestore';
import { useNavigate } from 'react-router-dom';
const CommunityModal = () => {
  const navigate = useNavigate();
  const modalData = useContext(ModalContext);
  const [data,setData]=useState("")
  const {isLoading,setIsLoading}=useContext(ModalContext);
  const { isOpen, onOpen, onClose } = useDisclosure();
  useEffect(() => {
    if (modalData.modal) {
      onOpen();
    }
    console.log("data")
  }, [modalData.modal, onOpen]);
  const handleClose = () => {
    onClose();
    modalData.setModal(false);
  };
  const createNewReddit=async()=>{
    try{
        setIsLoading(true);
        let docRef = await addDoc(collection(db,"subReddit"),{
            name:data,
        })
        const newSubReddit= await getDoc(docRef);
        console.log(newSubReddit.exists())
        if(!newSubReddit.exists()){
          throw new Error(`Sorry community is taken`);
        }
        const newData = newSubReddit.data();
       
        modalData.setCommunityData((prevData) => [...prevData, newData]);
        
        
        onClose();
        navigate(`/r/${data}`)
        setIsLoading(false);
        
        setData("");
    }
    catch(err){
        console.log(err.message)
        setIsLoading(false);
        onClose();
    } 
     
  }
  
  return (
    <Modal isOpen={isOpen} onClose={handleClose} size="lg">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create Community</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
           <Heading fontSize={"2rem"} mb="10px">Name</Heading>
           <Text mb="30px">Community names including capitalization cannot be changed.</Text>
           <Input variant='filled' placeholder='Enter new SubReddit' value={data} onChange={(e) => setData(e.target.value)} mb="20px" size='md' p="40px" height="20px"/>
           
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={handleClose}>
            Close
          </Button>
          <Button variant="ghost" onClick={()=>createNewReddit()} loadingText='Submitting' isLoading={isLoading}>Create</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default CommunityModal;
