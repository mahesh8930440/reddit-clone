import HeaderMenu from "../Components/HeaderMenu"
import { Flex, Spacer,InputGroup,InputLeftElement,Input,Image} from '@chakra-ui/react'
import { SearchIcon } from '@chakra-ui/icons'

import { FirebaseContext } from "../context/Firebase";
import {useContext} from "react";
import AuthenticationSection from "./AuthenticationSection";
const Header =()=>{
    const authData =useContext(FirebaseContext);
    const user=authData.user;
    
    return(
        <Flex h="100px" align="center"   justifyContent={"center"} pt={"5px"}>
        
            <Image src={"https://logowik.com/content/uploads/images/570_reddit.jpg"} alt="header-logo" h="80px"  mr="30px"/>
            <HeaderMenu/>
            
            <InputGroup ml="2" width='70%' mr="30px">
                <InputLeftElement  pointerEvents="none" color="gray.300" fontSize="1.2em" ml="3" children={<SearchIcon />} mr="30px"/>
                <Input placeholder="search .." pl="10" />
            </InputGroup>
            <AuthenticationSection width="40px"/>
         
           
        </Flex>
    )
}

export default Header;