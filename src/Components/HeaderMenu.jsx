import {Menu,MenuButton,MenuItem,Image,Button,MenuList,HStack,Text, Heading,Flex} from '@chakra-ui/react'
import { AddIcon,Icon} from '@chakra-ui/icons'
import { FaHome } from "react-icons/fa";
import { ChevronDownIcon } from "@chakra-ui/icons";
import {useContext,useEffect} from "react";
import { ModalContext } from '../context/community';
import CommunityModal   from "../Components/CommunityModal"
import {db} from "../context/Firebase";
import {collection ,getDocs} from 'firebase/firestore';
import {Link} from "react-router-dom"
const HeaderMenu=()=>{
    
    const modalData = useContext(ModalContext);
    
    
    
    useEffect(() => {
        const fetchCommunityData = async () => {
            try {
                const subRedditsSnapshot = await getDocs(collection(db, "subReddit"));
                const data = subRedditsSnapshot.docs.map((doc) => doc.data());
                modalData.setCommunityData(data);
            } catch (error) {
                console.log("Error fetching community data:", error);
            }
        };
         console.log("one")
        fetchCommunityData();
       
        
    }, []);
    
    
   
    return (<>
       <Menu mr="30px">
        <MenuButton as={Button} rightIcon={<ChevronDownIcon />} textAlign={"center"}>
            <HStack spacing="2">
                <Icon as={FaHome} boxSize={6} color="black" />
                <Text mr="30px">Home</Text>
            </HStack>
        </MenuButton>
        <MenuList> 
            <MenuItem minH='48px'>
                <AddIcon mr='12px'/>   
                <span onClick={()=>{modalData.setModal(true)}}>Create Community</span>
                {modalData.modal && (<CommunityModal />)}
            </MenuItem>
            <MenuItem minH='40px'>
                <Flex display="column">
                    <Heading mb="10px" fontSize={"1rem"}>YOUR COMMUNITIES</Heading>
                    {modalData.communityData.length > 0 && (
                       <Flex direction="column">
                       {modalData.communityData.map((community) => (
                         <Link to={`/r/${community.name}`} key={community.name}>
                           <Text width="100%" display="block" p="8px" >
                             {community.name}
                           </Text>
                         </Link>
                       ))}
                     </Flex>
                     
                    )}
                </Flex>
                

            </MenuItem>
            
            <Link to="/submit">
                <MenuItem minH='40px'>
                    <AddIcon mr='12px'/>
                    <span>Create Post</span>
                </MenuItem>
            </Link>
        </MenuList>
     </Menu>

     </>)
}
export default HeaderMenu;