import {createContext} from "react";
import {initializeApp} from "firebase/app";
import {getAuth,GoogleAuthProvider,signInWithPopup,onAuthStateChanged} from  'firebase/auth'
import {useState,useEffect} from "react";
import { getFirestore } from 'firebase/firestore';
import { getStorage } from "firebase/storage";



import { useToast } from "@chakra-ui/react";
const firebaseConfig = {
    apiKey: "AIzaSyC3-Ow2RnWbz3pu7FunoWZlUtaM9e_E3Hw",
    authDomain: "reddit-react-app-727a2.firebaseapp.com",
    projectId: "reddit-react-app-727a2",
    storageBucket: "reddit-react-app-727a2.appspot.com",
    messagingSenderId: "818999223926",
    appId: "1:818999223926:web:48445da021da80b5252157",
    measurementId: "G-4BYCGC1D0K"
  };
  
 

const app= initializeApp(firebaseConfig);
export const db =getFirestore(app);
export const storage = getStorage(app);
export const auth=getAuth(app);
const googleProvider =new GoogleAuthProvider();
export const FirebaseContext= createContext(null);

export const FirebaseProvider = (props)=>{
    const [user,setUser] =useState(null);
    const toast =useToast();
    const signInWithGoogle=async ()=>{
        try{
            await signInWithPopup(auth,googleProvider)
            toast({
                title: 'LogIn',
                description: 'You have been successfully logged .',
                status: 'success',
                duration: 2000,
                isClosable: true,
        })}
        catch(err){
            toast({
                title: 'Error',
                description: err.message,
                status: 'failed',
                duration: 2000,
                isClosable: true,
        }) 
        }
    
       
    }
    useEffect(()=>{
        const unsubscribe = onAuthStateChanged(auth,(user)=>{
            if(user){
                setUser(user);
            }
            else{
                setUser(null);
            }

        });
        return () => {
            unsubscribe();
        };
    },[])
    return (
         <FirebaseContext.Provider value={{signInWithGoogle,user}}>
            {props.children}
         </FirebaseContext.Provider>
    )
}