
import  {createContext} from "react";
import {useState} from "react";
import { collection, getDocs } from "firebase/firestore";
export const CommentContext=createContext(null);
import {db} from "./Firebase";
import {setDoc,doc, serverTimestamp} from 'firebase/firestore';
import {
   
    query,
    where,
    updateDoc,
  } from "firebase/firestore";
import {  useParams } from "react-router-dom";
import { auth } from './Firebase';
export const CommentProvider= (props)=>{
    
    
    const [commentsData, setCommentsData]=useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const user=auth.currentUser;
    
    let [value, setValue] = useState("")
    let handleInputChange = (e) => {
            let inputValue = e.target.value
            setValue(inputValue)
    }
    let handleComment=async (post)=>{
        try{
            
            setIsLoading(true)
            const {userId,postId,subredditName}=post;
            const commentId=new Date().getTime().toString();
            let result=await setDoc(doc(db,"comment",commentId),{
                userId,
                postId,
                user:user.displayName,
                commentId,
                description:value,
                subredditName,
                createdAt:serverTimestamp(),
                editedAt:serverTimestamp()
            })
            const postRef = doc(db, "post", postId);
            await updateDoc(postRef, {
             commentCount :post.commentCount+1, 
            });
            setValue("");
            if(result){
                console.log("successfull")
            }
            setIsLoading(false)
        }
        catch(err){
            console.log(err)
            setIsLoading(false)
        }
    }
    const fetchingComments=async(postId)=>{
       
     
        const commentQuery = query(
            collection(db, "comment"),
            where("postId", "==", postId)
          );
    
        const commentSnapshot = await getDocs(commentQuery);
        
        const commentsArray = [];
       commentSnapshot.forEach((doc) => {
     
           commentsArray.push(doc.data());
        });
        setCommentsData(commentsArray);
        
    }
    return (
        <CommentContext.Provider value={{commentsData,setCommentsData,isLoading,setIsLoading,value,setValue,handleComment,fetchingComments}}>
            {props.children}
        </CommentContext.Provider>
    )
} 