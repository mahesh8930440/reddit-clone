import  {createContext} from "react";
import {useState} from "react";

export const ModalContext=createContext(null);

export const ModalProvider= (props)=>{
    const [modal, setModal]=useState(false);
    const [communityData,setCommunityData]=useState([])
    const [postData,setPostData]=useState([])
    const [isLoading, setIsLoading] = useState(false);
    return (
        <ModalContext.Provider value={{modal,setModal,postData,setPostData,communityData,setCommunityData,isLoading,setIsLoading}}>
            {props.children}
        </ModalContext.Provider>
    )
}