import { createContext, useState } from "react";
import { db } from "../context/Firebase";

import {
  collection,
  query,
  where,
  getDocs,
  getDoc,
  addDoc,
  doc,
  setDoc,
  updateDoc,
} from "firebase/firestore";

export const VoteContext = createContext(null);

export const VoteProvider = (props) => {
  const [voteType, setVoteType] = useState(null);
  const [voteCount, setVoteCount] = useState(0);
  
  const handleVote = async (newVoteType,user, postId, voteStatus) => {
    try {
      if (!user) {
        console.log("User not logged in");
        return;
      }

      const voteQuery = query(
        collection(db, "votes"),
        where("userId", "==", user.uid),
        where("postId", "==", postId)
      );

      const voteSnapshot = await getDocs(voteQuery);
      if (!voteSnapshot.empty) {
        const voteData = voteSnapshot.docs[0].data();

        const voteRef = doc(db, "votes", user.uid + postId);
        console.log(voteStatus);
        console.log(voteType);
        if (newVoteType === voteData.voteType) {
          const newVoteCount = newVoteType === "upVote" ? -1 : 1;

          const updatedVoteStatus = voteStatus + newVoteCount;

          await updateDoc(voteRef, { voteType: null });
          setVoteType(null);
          const postRef = doc(db, "post", postId);

          await updateDoc(postRef, {
            voteStatus: updatedVoteStatus,
            voteType:null
          });
          setVoteCount(updatedVoteStatus);
        } else if (voteData.voteType === null) {
          const newVoteCount = newVoteType === "upVote" ? 1 : -1;
          const updatedVoteStatus = voteStatus + newVoteCount;

          await updateDoc(voteRef, { voteType: newVoteType });
          setVoteType(newVoteType);
          const postRef = doc(db, "post", postId);

          await updateDoc(postRef, {
            voteStatus: updatedVoteStatus,
            voteType:  newVoteType
          });
          setVoteCount(updatedVoteStatus);
        } else {
          const newVoteCount = newVoteType === "upVote" ? 2 : -2;
          const updatedVoteStatus = voteStatus + newVoteCount;

          await updateDoc(voteRef, { voteType: newVoteType });
          setVoteType(newVoteType);
          const postRef = doc(db, "post", postId);

          await updateDoc(postRef, {
            voteStatus: updatedVoteStatus,
            voteType:  newVoteType
          });
          setVoteCount(updatedVoteStatus);
        }
      } else {
        const newVoteCount = newVoteType === "upVote" ? 1 : -1;
        const updatedVoteCount = voteStatus + newVoteCount;
        setVoteCount(updatedVoteCount);
        const voteId = user.uid + postId;
        const ref = doc(db, "votes", voteId);
        await setDoc(ref, {
          userId: user.uid,
          postId: postId,
          voteType: newVoteType,
          voteId,
        });
        const postRef = doc(db, "post", postId);
        await updateDoc(postRef, {
          voteStatus: updatedVoteCount,
          voteType:  newVoteType
        });
      }
    } catch (error) {
      console.error("Error handling vote:", error);
    }

    setVoteType(newVoteType);
  };

  return (
    <VoteContext.Provider value={{ handleVote, voteType, voteCount }}>
      {props.children}
    </VoteContext.Provider>
  );
};
