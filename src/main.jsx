import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import { FirebaseProvider } from './context/Firebase.jsx'
import {ModalProvider} from "./context/community.jsx";
import appRouter from './App.jsx';
import {RouterProvider} from "react-router-dom"
import { VoteProvider } from './context/vote.jsx';
import { CommentProvider } from './context/comment.jsx';
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <FirebaseProvider>
      <ModalProvider>
        <VoteProvider>
          <CommentProvider>
           <RouterProvider router={appRouter}/>
          </CommentProvider>
           
        </VoteProvider>
      </ModalProvider>
    </FirebaseProvider>  
  </React.StrictMode>,
)
