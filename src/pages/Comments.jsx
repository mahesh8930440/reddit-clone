import { useParams } from "react-router-dom";
import { doc, getDoc } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../context/Firebase";
import { Avatar, Flex, Icon, Input } from "@chakra-ui/react";
import { LinkIcon } from "@chakra-ui/icons";
import { FirebaseContext } from "../context/Firebase";
import { IoChatboxOutline } from "react-icons/io5";
import { useContext } from "react";
import { BiDownvote } from "react-icons/bi";
import { BiUpvote } from "react-icons/bi";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  Box,
  Text,
  Stack,
  StackDivider,
  Textarea,
  Button,Image
} from "@chakra-ui/react";
import { CommentContext } from "../context/comment";
import { RxAvatar } from "react-icons/rx";
import { format } from "date-fns";
import { formatDistanceToNow } from "date-fns";
import { ModalContext } from "../context/community";

const Comments = () => {
  const { postId, subredditName } = useParams();
  const {user}=useContext(FirebaseContext)
  const [post, setPost] = useState([]);
  const [days, setDays] = useState(null);
  const [commentedDate,setCommentedDate] =useState(null);
  const {isLoading, setIsLoading}=useContext(ModalContext)
  const {setValue,handleComment,value,fetchingComments,commentsData}=useContext(CommentContext)
  useEffect(() => {
    const fetchingData = async () => {
      const docRef = doc(db, "post", postId);
      const data = await getDoc(docRef);

      if (data.exists()) {
        setPost(data.data());
      } else {
        console.log("No such document!");
      }
    };
    fetchingData();
  }, [setPost]);
  useEffect(() => {
    if (post && post.createdAt) {
      const dateObject = post.createdAt.toDate();
      const formattedDate = formatDistanceToNow(dateObject, {
        addSuffix: true,
      });
      setDays(formattedDate);
    }
    
  }, [post]);
  useEffect(()=>{
    
    fetchingComments(postId)
  },[value]) 
  return (
    <Box   maxW="40%" mx="auto">
      {post && (
        <Flex my="30px" pt="40px"  mx="auto">
          <Card size="md" mb="20px" width="100%">
            <Flex>
              <Box id="post.postId" my="10px" bg="whitesmoke" p="10px">
                <BiUpvote
                  size="30px"
                  color={post.voteType === "upVote" ? "red" : "gray"}
                  onClick={() =>
                    handleVote("upVote", user, post.postId, post.voteStatus)
                  }
                />
                <Text id="post.postId" textAlign={"center"}>
                  {post.voteStatus}
                </Text>
                <BiDownvote
                  size="30px"
                  color={post.voteType === "downVote" ? "red" : "gray"}
                  onClick={() =>
                    handleVote("downVote", user, post.postId, post.voteStatus)
                  }
                />
              </Box>
              <Box display={"column"}>
                <CardHeader>
                  <Flex justifyContent={"space-between"}>
                    <Flex mr="20px">
                      <Avatar bg="teal.500" h="25px" w="25px" mr="10px" src="https://wallpaperaccess.com/full/2213441.jpg"/>
                      <Heading size="sm">{post.subredditName}</Heading>
                    </Flex>

                    <Text mr="20px" color="gray" size="sm">Post by {post.userName}</Text>
                    <Text color="gray" size="sm">{days}</Text>
                  </Flex>
                </CardHeader>

                <CardBody>
                  <Stack divider={<StackDivider />} spacing="4" mb="20px">
                    <Box>
                      <Heading size="md" textTransform="uppercase">
                        {post.title}
                      </Heading>
                      <Text pt="2" fontSize="md">
                            {post.description && post.description.startsWith("http") ? (
                              <a href={post.description} target="_blank" rel="noopener noreferrer">
                                {post.description}
                              </a>
                            ) : (
                              <>
                                <Text pt="2" fontSize="md">
                                  {post.description}
                                </Text>
                                {post.imageUrl && <Image src={post.imageUrl} alt="Image" />}
                              </>
                            )}
                          </Text>
                    </Box>
                  </Stack>
                  <Flex>
                    <Icon as={IoChatboxOutline} mr="10px" />
                    <Text color={"gray"}>{commentsData.length} Comment</Text>
                  </Flex>
                </CardBody>
                
                <CardBody >
                {user && <Text mb='8px'>comment as    <Box as="span" color="pink">{user.displayName}</Box></Text>}
                <Textarea
                  value={value}
                  onChange={(e)=>  setValue(e.target.value)}
                  placeholder='what are you thoughts ?'
                  size="md"
                  mb="40px"
                  variant='filled'
                />
                <Button colorScheme='blue' onClick={()=>handleComment(post)} loadingText="loading" isLoading={isLoading}>comment</Button>
                </CardBody>
              </Box>
            </Flex>
          </Card>
        </Flex>
      )}
      <Box p='30px' bg="white" borderRadius={"md"}>
        
      { commentsData.map((comment) =>{
        return(<>
          <Flex mr="20px">
                <Avatar bg="teal.500" h="25px" w="25px" mr="10px" src="https://enviragallery.com/wp-content/uploads/2015/12/background-lighting.jpg"/>
                <Heading size="md" mr="10px">{comment.user}</Heading>
                
          </Flex>
          <Text mb="20px" ml="40px">{comment.description}</Text>
       </>)})
       
      }
      </Box>
    </Box>
  );
};
export default Comments;
