import { Avatar, Heading ,Text} from "@chakra-ui/react"
import { v4 as uuidv4 } from 'uuid';
import {
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    Button,
    Container,
    InputLeftElement,
    InputGroup,
    Input,MenuGroup,
    Flex,
    Textarea,ButtonGroup,
    Select,
    
  } from '@chakra-ui/react'
import { Tabs, TabList, TabPanels, Tab, TabPanel,Box } from '@chakra-ui/react'
import {useContext,useEffect} from "react";
import { ModalContext } from '../context/community';
import { FirebaseContext } from "../context/Firebase"
import { HStack, Icon } from "@chakra-ui/react";
import { FaExternalLinkAlt } from "react-icons/fa";
import {useState} from "react";
import { SearchIcon,ChevronDownIcon,LinkIcon } from "@chakra-ui/icons"
import { useNavigate } from "react-router-dom";
import {db} from "../context/Firebase";
import { storage } from "../context/Firebase";
import { ref , uploadBytes, getDownloadURL } from "firebase/storage";

import { setDoc,doc, collection ,getDocs, serverTimestamp} from 'firebase/firestore';


const CreatePost =()=>{
  const navigate = useNavigate();
  const {user}=useContext(FirebaseContext) ;
  const [title,setTitle]=useState("");
  const [text,setText] =useState("");
  const [file, setFile] = useState(null);
  
  const [selectedPostType, setSelectedPostType] = useState("Post");
  
  const [selectedCommunity,setSelectedCommunity] =useState("");
  const {communityData,setPostData,postData,isLoading,setIsLoading}=useContext(ModalContext);
  

  
  const handlePost = async () => {
    try {
      setIsLoading(true);
      const postId = new Date().getTime().toString();
      const reference = doc(db, "post", postId);
  
      let postData = {
        postId,
        title,
        userId: user.uid,
        userName: user.displayName,
        subredditName: selectedCommunity,
        createdAt: serverTimestamp(),
        editedAt: serverTimestamp(),
        voteStatus: 0,
        commentCount: 0,
        voteType: null,
      };
      
      const uploadFileToStorage = async (file) => {
        try {
          
          const storageRef = ref(storage, `uploads/${uuidv4()}`);
           
          const uploadTask = await uploadBytes(storageRef, file);
        
          const snapshot = await getDownloadURL(uploadTask.ref);
          
          return snapshot;
        } catch (error) {
          console.error("Error uploading file:", error.message);
          throw error;
        }
      };
      if (selectedPostType === "Post") {
        postData = { ...postData, description: text };
      } else if (selectedPostType === "Upload") {
         if (file){
          
          const fileUrl = await uploadFileToStorage(file);
          postData = { ...postData, imageUrl: fileUrl };
         }
            
      } else if (selectedPostType === "Link") {
        postData = { ...postData, description: text };
      }
  
      await setDoc(reference, postData);
      setIsLoading(false);
      setSelectedCommunity("");
      setText("");
      setTitle("");
      navigate("/");
  
      const newPost = await getDocs(reference);
      const newData = newPost.data();
      setPostData((prevData) => [...prevData, newData]);
    } catch (err) {
      console.log(err.message);
      setIsLoading(false);
    }
  };
 
  
  const handleFileUpload = (e) => {
    const uploadedFile = e.target.files[0];
    setFile(uploadedFile);
  };
  return (
    <Container  pt="30px"  maxW='2xl' >
      <Box bg="white" p="30px" borderRadius={"md"}>
        <Heading mb="20px" fontSize={"1.5rem"}>Create a Post</Heading>
        <Select placeholder='Choose Your Community' value={selectedCommunity} mb="30px" onChange={(e)=> setSelectedCommunity(e.target.value)}>
            {communityData.length > 0 && (
                communityData.map((community) => (
                    <option value={community.name}>
                        {community.name}
                    </option>
                ))
            )}
        </Select>
        
        <Tabs isFitted variant='enclosed'>
          <TabList mb='1em'>
            <Tab onClick={() => setSelectedPostType("Post")} isSelected={selectedPostType === "Post"}>
              <HStack spacing={2}>
                <Icon as={FaExternalLinkAlt} boxSize={4} />
                <Text>Post</Text>
              </HStack>
            </Tab>
            <Tab onClick={() => setSelectedPostType("Upload")} isSelected={selectedPostType === "Upload"}>
              <HStack spacing={2}>
                <LinkIcon alignSelf={"center"} />
                <Text>Upload</Text>
              </HStack>
            </Tab>
            <Tab onClick={() => setSelectedPostType("Link")} isSelected={selectedPostType === "Link"}>
              <HStack spacing={2}>
                <LinkIcon alignSelf={"center"} />
                <Text>Link</Text>
              </HStack>
            </Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <Input placeholder='Title' mb="20px" value={title} onChange={(e) => setTitle(e.target.value)} />
              <Textarea placeholder='Text' mb="20px" value={text} onChange={(e) => setText(e.target.value)} />
            </TabPanel>
            <TabPanel>
              <Input placeholder='Title' mb="20px" value={title} onChange={(e) => setTitle(e.target.value)} />
              <Input type="file" mb="20px" onChange={(e) => handleFileUpload(e)} />
            </TabPanel>
            <TabPanel>
              <Input placeholder='Title' mb="20px" value={title} onChange={(e) => setTitle(e.target.value)} />
              <Input placeholder='URL' mb="20px" value={text}  onChange={(e) => setText(e.target.value)}/>
            </TabPanel>
          </TabPanels>
        </Tabs>
        <ButtonGroup variant='outline' spacing='6'  >
            <Button colorScheme='blue'  onClick={() => handlePost()} loadingText="loading" isLoading={isLoading} >Save</Button>
            <Button onClick={()=> navigate("/")}>Cancel</Button>
        </ButtonGroup>
      </Box>  
    </Container>
  )
}

export default CreatePost;