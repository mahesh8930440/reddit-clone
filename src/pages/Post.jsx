import {
  Avatar,
  AvatarBadge,
  Container,
  Flex,
  Icon,
  Input,
} from "@chakra-ui/react";
import CreatePost from "./CreatePost";
import { LinkIcon } from "@chakra-ui/icons";
import { Link, useParams } from "react-router-dom";
import { IoChatboxOutline } from "react-icons/io5";
import { ModalContext } from "../context/community";
import { BiDownvote } from "react-icons/bi";
import { BiUpvote } from "react-icons/bi";
import { useContext, useEffect } from "react";

import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  Box,
  Text,
  Stack,
  StackDivider,
  Button,Image
} from "@chakra-ui/react";
import { db } from "../context/Firebase";
import { addDoc, collection, getDocs } from "firebase/firestore";
import { RxAvatar } from "react-icons/rx";
import { format } from "date-fns";
import { formatDistanceToNow } from "date-fns";
import { FirebaseContext } from "../context/Firebase";
import { VoteContext } from "../context/vote";
import { CommentContext } from "../context/comment";
import { useState } from "react";

const Posts = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [sortedPosts, setSortedPosts] = useState([]);
    const itemsPerPage = 5;
    const { setPostData, postData } = useContext(ModalContext);
    const { user } = useContext(FirebaseContext);
    const { handleVote, voteType } = useContext(VoteContext);
    const { commentsData } = useContext(CommentContext);
   
    
    useEffect(() => {
      const fetchPostData = async () => {
        try {
          const postSnapshot = await getDocs(collection(db, "post"));
          const posts = postSnapshot.docs.map((doc) => doc.data());
          setPostData(posts);
          setSortedPosts(posts.slice());
        } catch (error) {
          console.log("Error fetching post data:", error);
        }
      };
      fetchPostData();
      console.log("data")
    }, [setPostData,voteType]);
   

  
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const currentPostItems = sortedPosts.slice(startIndex, endIndex);
    const handleLatestPost = () => {
      const sortedPostData = postData.slice().sort((newPost, oldPost) => oldPost.createdAt - newPost.createdAt);
      setSortedPosts(sortedPostData);
      console.log("latest");
    };
  
    const handlePopularPost = () => {
      const sortedPostData = postData.slice().sort((post, old) => old.voteStatus - post.voteStatus);
      setSortedPosts(sortedPostData);
      console.log("votes");
    };
  
    const totalPages = Math.ceil(postData.length / itemsPerPage);
    
    const handlePageChange = (newPage) => {
      if (newPage >= 1 && newPage <= totalPages) {
        setCurrentPage(newPage);
      }
    };
  
    
  
  return (
    <Box py="2%"  maxW="40%" mx="auto">
      <Flex justifyContent="space-between"  mb="40px" bgColor={"white"} p={"20px"} borderRadius="md">
        <Avatar mr="30px" src="https://wallpaperaccess.com/full/2213441.jpg">
          <AvatarBadge boxSize="1.25em" bg="green.500" />
        </Avatar>
        <Link to="/submit">
          <Input
            placeholder="Create Post"
            size="lg"
            mr="30px"
            p="10px"
            w="30vw"
          />
        </Link>
        <LinkIcon alignSelf={"center"} />
      </Flex>
      <Flex  justifyContent={"center"}>
        <Button mx="20px" onClick={handleLatestPost}>Latest</Button>
        <Button mx="20px" onClick={handlePopularPost}>Popular</Button>
      </Flex>
      <Flex direction={"column"} spacing="4" mt="20px" size="lg" w="100%">
        {currentPostItems.map((data) => {
          const dateObject = data.createdAt.toDate();
          const formattedDate = formatDistanceToNow(dateObject, {
            addSuffix: true,
          });

          return (
            <Card size="md" mb="20px" width="100%">
              <Flex>
                <Box id="data.postId" my="10px" bg="whitesmoke" p="10px">
                  <BiUpvote
                    size="30px"
                    color={data.voteType === "upVote" ? "red" : "gray"}
                    onClick={() =>
                      handleVote("upVote",user, data.postId, data.voteStatus)
                    }
                  />
                  <Text id="data.postId" textAlign={"center"}>
                    {data.voteStatus}
                  </Text>
                  <BiDownvote
                    size="30px"
                    color={data.voteType === "downVote" ? "red" : "gray"}
                    onClick={() =>
                      handleVote("downVote",user, data.postId, data.voteStatus)
                    }
                  />
                </Box>
                <Box display={"column"}>
                  <CardHeader>
                    <Flex justifyContent={"space-between"}>
                      <Flex mr="20px">
                        <Avatar bg="teal.500" h="25px" w="25px" mr="10px" src="https://enviragallery.com/wp-content/uploads/2015/12/background-lighting.jpg"/>
                        <Heading size="sm" fontSize="md">{data.subredditName}</Heading>
                      </Flex>

                      <Text mr="20px" fontSize="md" textColor={"gray"}>Post by {data.userName}</Text>
                      <Text textColor={"gray"}>{formattedDate}</Text>
                    </Flex>
                  </CardHeader>

                  <CardBody>
                    <Stack divider={<StackDivider />} spacing="2">
                      <Box>
                        <Heading size="md" textTransform="uppercase">
                          {data.title}
                        </Heading>
                       
                        <Text pt="2" fontSize="md">
                          {data.description && data.description.startsWith("http") ? (
                            <a href={data.description} target="_blank" rel="noopener noreferrer">
                              {data.description}
                            </a>
                          ) : (
                            <>
                              <Text pt="2" fontSize="md">
                                {data.description}
                              </Text>
                              {data.imageUrl && <Image src={data.imageUrl} alt="Image" />}
                            </>
                          )}
                        </Text>
                                                

                      </Box>
                    </Stack>
                  </CardBody>
                  <CardBody>
                    <Link to={`/r/${data.subredditName}/comment/${data.postId}`}>
                        <Flex >
                            <Icon as={IoChatboxOutline} mr="10px" size={"md"}/>
                            <Text textAlign={"center"}>{data.commentCount} Comment</Text>
                        </Flex>
                    </Link>
                    
                  </CardBody>
                </Box>
              </Flex>
            </Card>
          );
        })}
      </Flex>
      <Box color={"white"} mx="24px" my="24px">
        <Button disabled={currentPage === 1} mx="24px" onClick={() => handlePageChange(currentPage - 1)}>
          Previous
        </Button>
        <span mx="24px">{`Page ${currentPage} of ${totalPages}`}</span>
        <Button disabled={currentPage === totalPages} mx="24px" onClick={() => handlePageChange(currentPage + 1)}>
          Next
        </Button>
      </Box>
     
    </Box>
  );
};
export default Posts;
