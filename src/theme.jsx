

export const Button = {
  variants: {
    solid: {
      color: "white",
      bg: "blue.500",
      _hover: {
        bg: "blue.400",
      },
    },
    outline: {
      color: "blue",
      border: "1px solid",
      borderColor: "blue.500",
    },
  },
};
